import os
import envyaml

from analysis_tools.utils import import_root
ROOT = import_root()

import correctionlib
correctionlib.register_pyroot_binding()

corrCfg = envyaml.EnvYAML('%s/src/Corrections/EGM/python/eleCorrectionsFiles.yaml' % 
                                    os.environ['CMSSW_BASE'])

class eleSFRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.wps = kwargs.pop("wps")

        prefix = "" ; isUL = False
        try:
            isUL = kwargs.pop("isUL")
            prefix += "" if not isUL else "UL"
        except KeyError:
            pass
        try:
            prefix += kwargs.pop("runPeriod")
        except KeyError:
            pass

        self.corrKey = prefix + year

        if self.isMC:
            if not isUL and self.year < 2022:
                raise ValueError("Only implemented for Run2 UL datasets")

            if not os.getenv("_corr"):
                os.environ["_corr"] = "_corr"

                if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                    ROOT.gInterpreter.Load("libBaseModules.so")

                ROOT.gInterpreter.Declare(os.path.expandvars(
                    '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

            if not os.getenv(f"_eleSF_{self.corrKey}"):
                os.environ[f"_eleSF_{self.corrKey}"] = "_eleSF"
                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SF"][self.corrKey]["fileName"],
                            corrCfg["SF"][self.corrKey]["corrName"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_ele_sf_%s(
                            std::string syst, std::string wp, Vfloat eta, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else sf.push_back(corr_%s.eval({"%s", syst, wp, eta[i], pt[i]}));
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, corrCfg["SF"][self.corrKey]["yearkey"]))

    def run(self, df):
        if not self.isMC:
            return df, []

        branches = []
        for syst_name, syst in [("", "sf"), ("_up", "sfup"), ("_down", "sfdown")]:
            for wp in self.wps:
                df = df.Define("elesf_%s%s" % (wp, syst_name),
                    'get_ele_sf_%s("%s", "%s", Electron_eta, Electron_pt)' %
                    (self.corrKey, syst, wp))

                branches.append("elesf_%s%s" % (wp, syst_name))

        return df, branches


class eleSSRDFProducer():
    def __init__(self, *args, **kwargs):
        self.isMC = kwargs.pop("isMC")
        self.year = kwargs.pop("year") ; year = str(self.year)
        self.prefix = kwargs.pop("runPeriod")
        self.corrKey = self.prefix + year

        if not os.getenv("_corr"):
            os.environ["_corr"] = "_corr"
            if "/libBaseModules.so" not in ROOT.gSystem.GetLibraries():
                ROOT.gInterpreter.Load("libBaseModules.so")
            ROOT.gInterpreter.Declare(os.path.expandvars(
                '#include "$CMSSW_BASE/src/Base/Modules/interface/correctionWrapper.h"'))

        if not self.isMC:
            if self.year != 2022:
                raise ValueError("Only needed for Run3 2022 data datasets for now")

            if not os.getenv(f"_eleSS_data_{self.corrKey}"):
                os.environ[f"_eleSS_data_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameScale"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    ROOT::RVec<double> get_ele_ss_%s(
                            std::string syst, Double_t run,
                            Vint gain, Vfloat eta, Vfloat r9, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else {
                                if (syst == "down") {
                                    sf.push_back(corr_%s.eval({"total_correction", gain[i], run, eta[i], r9[i], pt[i]}) -
                                        corr_%s.eval({"total_uncertainty", gain[i], run, eta[i], r9[i], pt[i]}));
                                }
                                else if (syst == "up") {
                                    sf.push_back(corr_%s.eval({"total_correction", gain[i], run, eta[i], r9[i], pt[i]}) +
                                        corr_%s.eval({"total_uncertainty", gain[i], run, eta[i], r9[i], pt[i]}));
                                }
                                else {
                                    sf.push_back(corr_%s.eval({"total_correction", gain[i], run, eta[i], r9[i], pt[i]}));
                                }
                            }
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, self.corrKey, self.corrKey,
                       self.corrKey, self.corrKey))

        else:
            if self.year != 2022:
                raise ValueError("Only needed for Run3 2022 MC datasets for now")

            if not os.getenv(f"_eleSS_mc_{self.corrKey}"):
                os.environ[f"_eleSS_mc_{self.corrKey}"] = "_eleSS"

                ROOT.gInterpreter.ProcessLine(
                    'auto corr_%s = MyCorrections("%s", "%s");' %
                        (self.corrKey, corrCfg["SS"][self.corrKey]["fileName"],
                            corrCfg["SS"][self.corrKey]["corrNameSmear"]))

                ROOT.gInterpreter.Declare("""
                    using Vfloat = const ROOT::RVec<float>&;
                    using Vint = const ROOT::RVec<int>&;
                    TRandom rndm_%s(7);
                    ROOT::RVec<double> get_ele_ss_%s(
                            std::string syst, Vfloat eta, Vfloat r9, Vfloat pt) {
                        ROOT::RVec<double> sf;
                        for (size_t i = 0; i < pt.size(); i++) {
                            if (pt[i] < 10.) sf.push_back(1.);
                            else {
                                if (syst == "down") {
                                    Double_t smearing = corr_%s.eval({"rho", eta[i], r9[i]});
                                    Double_t uncrtnty = corr_%s.eval({"err_rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing - uncrtnty));
                                }
                                else if (syst == "up") {
                                    Double_t smearing = corr_%s.eval({"rho", eta[i], r9[i]});
                                    Double_t uncrtnty = corr_%s.eval({"err_rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing + uncrtnty));
                                }
                                else {
                                    Double_t smearing = corr_%s.eval({"rho", eta[i], r9[i]});
                                    sf.push_back(rndm_%s.Gaus(1., smearing));
                                }
                            }
                        }
                        return sf;
                    }
                """ % (self.corrKey, self.corrKey, self.corrKey, self.corrKey, self.corrKey,
                    self.corrKey, self.corrKey, self.corrKey, self.corrKey, self.corrKey))


    def run(self, df):
        if not self.isMC:
            branches = []
            for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
                df = df.Define("eless%s" % syst_name, 
                                'get_ele_ss_%s("%s", run, Electron_seedGain, Electron_eta, Electron_r9, Electron_pt)'
                                % (self.corrKey, syst))

                df = df.Define("Electron_pt_corr%s" % syst_name, 
                                "Electron_pt * eless%s" % syst_name)

                branches.append("eless%s" % syst_name)
                branches.append("Electron_pt_corr%s" % syst_name)

            return df, branches

        else:
            branches = []
            for syst_name, syst in [("", "nom"), ("_up", "up"), ("_down", "down")]:
                df = df.Define("eless%s" % syst_name, 
                    'get_ele_ss_%s("%s", Electron_eta, Electron_r9, Electron_pt)'
                    % (self.corrKey, syst))

                df = df.Define("Electron_pt_corr%s" % syst_name, 
                    "Electron_pt * eless%s" % syst_name)

                branches.append("eless%s" % syst_name)
                branches.append("Electron_pt_corr%s" % syst_name)

            return df, branches


def eleSFRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    :param wps: name of the wps to consider among ``Loose``, ``Medium``,
        ``RecoAbove20``, ``RecoBelow20``, ``Tight``, ``Veto``, ``wp80iso`` (default),
        ``wp80noiso``, ``wp90iso,`` ``wp90noiso``.
    :type wps: list of str

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSFRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
                isUL: self.dataset.has_tag('ul')
                wps: [wp80iso, ...]
    """

    return lambda: eleSFRDFProducer(**kwargs)

def eleSSRDF(**kwargs):
    """
    Module to obtain electron SFs with their uncertainties.

    YAML sintaxis:

    .. code-block:: yaml

        codename:
            name: eleSSRDF
            path: Corrections.EGM.eleCorrections
            parameters:
                isMC: self.dataset.process.isMC
                year: self.config.year
                runPeriod: self.dataset.runPeriod
    """

    return lambda: eleSSRDFProducer(**kwargs)

