import os
import ROOT
ROOT.gROOT.SetBatch(True)

from Corrections.EGM.eleCorrections import eleSFRDF, eleSSRDF
test_folder = "$CMSSW_BASE/src/Base/Modules/data/"

def ele_sf_test_run2(df, isMC, year, isUL, runPeriod):
    elesf_UL16preVFP = eleSFRDF(
        isMC=isMC,
        year=year,
        isUL=isUL,
        runPeriod=runPeriod,
        wps=["wp80iso"]
    )()
    df, _ = elesf_UL16preVFP.run(df)
    h = df.Histo1D("elesf_wp80iso")
    print(f"Electron {year}{runPeriod} wp80iso SF Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def ele_sf_test_run3(df, isMC, year, runPeriod):
    elesf = eleSFRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
        wps=["wp80iso"]
    )()
    df, _ = elesf.run(df)
    h = df.Histo1D("elesf_wp80iso")
    print(f"Electron {year}{runPeriod} wp80iso SF Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df

def ele_ss_test_run3(df, isMC, year, runPeriod):
    eless = eleSSRDF(
        isMC=isMC,
        year=year,
        runPeriod=runPeriod,
    )()
    df, _ = eless.run(df)
    h = df.Histo1D("eless")
    print(f"Electron {year}{runPeriod} SS Integral: %.3f, Mean: %.3f, Std: %.3f" % 
            (h.Integral(), h.GetMean(), h.GetStdDev()))
    return df


if __name__ == "__main__":
    df_mc2016preVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016.root")))
    _ = ele_sf_test_run2(df_mc2016preVFP, True, 2016, True, "preVFP")

    df_mc2016postVFP = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2016_apv.root")))
    _ = ele_sf_test_run2(df_mc2016postVFP, True, 2016, True, "postVFP")

    df_mc2017 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2017.root")))
    _ = ele_sf_test_run2(df_mc2017, True, 2017, True, "")

    df_mc2018 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2018.root")))
    _ = ele_sf_test_run2(df_mc2018, True, 2018, True, "")

    df_mc2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022.root")))
    _ = ele_sf_test_run3(df_mc2022, True, 2022, "preEE")
    _ = ele_ss_test_run3(df_mc2022, True, 2022, "preEE")

    df_mc2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_mc2022_postee.root")))
    _ = ele_sf_test_run3(df_mc2022_postEE, True, 2022, "postEE")
    _ = ele_ss_test_run3(df_mc2022_postEE, True, 2022, "postEE")

    df_data2022 = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2022.root")))
    _ = ele_ss_test_run3(df_data2022, False, 2022, "preEE")

    df_data2022_postEE = ROOT.RDataFrame("Events", os.path.expandvars(os.path.join(test_folder, "testfile_data2022_postee.root")))
    _ = ele_ss_test_run3(df_data2022_postEE, False, 2022, "postEE")


